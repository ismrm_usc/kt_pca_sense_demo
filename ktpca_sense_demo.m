clear all;
clc;
close all;

%% load raw data

dr= rawloadHD_si('P34304.7');

opxres = 224;	% frequency encoding direction
opyres = 192;	% phase encoding direction
rhnframes = 29;	% time frame number
ncoil=8;

temp = reshape(dr,opxres,opyres+1,rhnframes,ncoil);
kdata0 = temp(:,2:end,:,:);	

kdata0=permute(kdata0,[2 1 3 4]);% k-space data: FE*PE*FRAME*Ncoil

%% parameters
ncoil=7; % #8: not a good coil
opxres = 224;	% frequency encoding direction
opyres = 192;	% phase encoding direction
rhnframes = 29;	% time frame number

thresh=0.004; % threshold used in SVD(obtaining basis function from training data)

R=4; %acceleration factor
% idx_re=load('idx_re.mat'); %sampling mask

%% sampling: training data
num_tra=16; % number of training lines
tra_line=(-8:7)+opyres/2+1; % index of training lines

kdata_tra=zeros(num_tra,opxres,rhnframes,ncoil);
im_tra=zeros(num_tra,opxres,rhnframes,ncoil);

im_tra_sum=zeros(num_tra,opxres,rhnframes);
T0=zeros(num_tra,opxres,rhnframes);

for nn_frame=1:rhnframes

    for nn_coil=1:ncoil
        
        kdata_tra(:,:,nn_frame,nn_coil)=kdata0(tra_line,:,nn_frame,nn_coil);

        im_tra(:,:,nn_frame,nn_coil)=ift(kdata_tra(:,:,nn_frame,nn_coil));

    end
    
    tmp=squeeze(im_tra(:,:,nn_frame,:));
    
    im_tra_sum(:,:,nn_frame)=sqrt(sum(tmp.*conj(tmp),3)/ncoil);

end

%% training
T=reshape(im_tra_sum,num_tra*opxres,rhnframes);
%SVD
disp('SVD');
[u,s,v] = svd(T,0);
s= diag(s);
sN =s/max(abs(s));
idxS = sN > thresh;
vpp = v(:,idxS);
nV = sum(idxS);

Basis_tem=vpp;
npc=nV;

%% Sensitivity map
nn_frame=1; % assume: smaps time-invariant
[S_map,ACS_im,Phi]=pmri_sensitivity_map(squeeze(kdata0(:,:,nn_frame,:)),tra_line,0);

% [S_map,ACS_im,Phi]=pmri_sensitivity_map(kdata,ACS_idx,flag_map_smooth)
for nn_coil=1:ncoil
    subplot(1,ncoil,nn_coil);
    imshow(abs(S_map(:,:,nn_coil)),[]);
    axis equal;axis tight;
end
        
% %% Alternative Sensitivity map: using espirit
% ncoil=7;
% nn_frame=1; 
% calib=squeeze(kdata_tra(:,:,nn_frame,1:ncoil));
% calib=permute(calib,[2 1 3]);
% 
% DATA=squeeze(kdata0(:,:,nn_frame,1:ncoil));
% DATA=permute(DATA,[2 1 3]);
% 
% ksize=[6,6];
% DATA_size=[opxres,opyres,ncoil];
% 
% 
% S_map=espirit_maps(DATA,DATA_size,ksize,calib);
% 
% S_map=permute(S_map,[2 1 3]);
% 
% for nn_coil=1:ncoil
%     subplot(1,ncoil,nn_coil);
%     imshow(abs(S_map(:,:,nn_coil)),[]);
%     axis equal;axis tight;
% end
%% sampling: recon data

idx_re=zeros(opyres,opxres,rhnframes);
idx_re=create_sample_pattern(opyres,opxres,rhnframes,R);

kdata_re=zeros(opyres,opxres,rhnframes,ncoil);
im_re=zeros(opyres,opxres,rhnframes,ncoil);

for nn_coil=1:ncoil
for nn_frame=1:rhnframes

        kdata_re(:,:,nn_frame,nn_coil)=kdata0(:,:,nn_frame,nn_coil).*idx_re(:,:,nn_frame);
        
        im_re(:,:,nn_frame,nn_coil)=ift(kdata_re(:,:,nn_frame,nn_coil));
end
end

psf=ift(squeeze(idx_re(:,1,:)));

figure;
subplot(1,3,1);
imshow(squeeze(idx_re(:,1,:)));
title('k-t sampling')
subplot(1,3,2);
imshow(abs(psf),[]);
subplot(1,3,3);
imshow(abs(im_re(:,:,1,1)),[]);

%% kt-PCA recon
%% step1: create v'*A'*b: right side of the equation

% coil combination
im_sum_re=zeros(opyres,opxres,rhnframes);

for nn_coil=1:ncoil
    
    im_sum_re=im_sum_re+im_re(:,:,:,nn_coil).*conj(repmat(S_map(:,:,nn_coil),[1 1 rhnframes]));

end

tmp=reshape(im_sum_re,opyres*opxres,rhnframes);
im_sum_re0=tmp.';

b_real=Basis_tem'*im_sum_re0; %dimension: npc*(nx*ny)

b_real=b_real(:);
%% step2: solving (v'*A'*A*v )*u'= v'*A'*b using Conjugate Gradient

savefile = 'kt_pcadata.mat';
save(savefile, 'Basis_tem','idx_re','S_map');

init_ut=zeros(npc*opyres*opxres,1);
[ut,iter_total,resvec] = CG_xm1(b_real,init_ut);
%   iter_total: total number of iterations
%   resvec: norm(gradient) after every iter

%% ut: 
UT=reshape(ut,npc,opyres*opxres);

Im_recon=UT'*Basis_tem';

Im_recon=reshape(Im_recon,opyres,opxres,rhnframes);


figure;

for nn_frame=1:rhnframes
        
        imshow(abs(Im_recon(:,:,nn_frame)),[]);
        
        M(nn_frame)=getframe;

end
movie(M,5);


