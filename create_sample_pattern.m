function idx=create_sample_pattern(opyres,opxres,rhnframes,R)
% INPUT:
% opxres:frequency encoding direction
% opyres:phase encoding direction
% R:acceleration factor

% OUTPUT:
% idx=opyres*opxres*rhnframes

sample_pattern=zeros(opyres,opxres,R);
sample_vector=zeros(opyres,R);
for r=1:R
    sample_idx=r:R:(floor(opyres/R)-1)*R+r;
    sample_vector(sample_idx,r)=ones(length(sample_idx),1);
    
    sample_pattern(:,:,r)=repmat(sample_vector(:,r),1,opxres);
end


idx=zeros(opyres,opxres,rhnframes);

for nn_frame=2:rhnframes+1

        idx(:,:,nn_frame-1)=sample_pattern(:,:,mod(nn_frame,R)+1);
 
end

end