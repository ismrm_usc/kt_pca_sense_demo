function [S_map,ACS_im,Phi]=pmri_sensitivity_map(kdata,ACS_idx,flag_map_smooth)
%
%	pmri_sensitivity_map   obtain sensitivity map for SENSE reconstruction
%
%
%	S_map=pmri_sensitivity_map(kdata,ACS_idx,flag_map_smooth);
%
%	INPUT:
%	kdata: zero-filled k-space data[n_PE, n_FE, n_chan]. the center of
%	k-space data is fully-sampled.
%		n_PE: # of phase encoding before acceleration
%		n_FE: # of frequency encoding
%		n_chan: # of channel
%	ACS_idx: index of fully-sampled lines in the center of k-space data . 
%	flag_map_smooth: value of either 0 or 1
%		It indicates of smoothing sensitivity map or not.
%	
%
%	OUTPUT:
%	S_map: coil sensitivity maps of [n_PE, n_FE, n_chan].
%		n_PE: # of phase encoding
%		n_FE: # of frequency encoding
%		n_chan: # of channel
%   Phi: noise covariance matrix [Ncoil Ncoil]


Ny=size(kdata,1);
Nx=size(kdata,2);
Ncoil=size(kdata,3);

%ACS_im: fully-sampled, low-resolution image by Ncoil coils; 
ACS_im=zeros(Ny,Nx,Ncoil);
ACS_data=zeros(Ny,Nx,Ncoil);

for nn_coil=1:Ncoil
    ACS_data(ACS_idx,:,nn_coil)=kdata(ACS_idx,:,nn_coil); 
    
    ACS_im(:,:,nn_coil) = ift(ACS_data(:,:,nn_coil));
end

S_map=zeros(Ny,Nx,Ncoil);

sum_im = sqrt(sum(ACS_im.*conj(ACS_im),3)/Ncoil);

if (flag_map_smooth)
    mask_ref = sum_im > 0.1.*max(sum_im(:));  %% a mask

    [dd,S_map]=pmri_poly_sensitivity(ACS_im,3,'mask',mask_ref,'flag_reim',0,'flag_maph',1);
else
    for nn_coil=1:Ncoil
        S_map(:,:,nn_coil)=ACS_im(:,:,nn_coil)./sum_im;
    end
end

%use sum_im to find the noise index : only include the noise
%mask=zeros(size(sum_im));
noise_idx=find(abs(sum_im)<mean(abs(sum_im(:)))/10);

%only get the real part of noise samples(Gaussian distribution) instead of magnitude
noiseimg=zeros(length(noise_idx),Ncoil);

for nn_coil=1:Ncoil
    tmp=ACS_im(:,:,nn_coil);
    noiseimg(:,nn_coil)=real(tmp(noise_idx));
end

%obtain the noise correlation matrices (N_coil * N_coil)
Phi=noiseimg'*noiseimg;
Phi=inv(Phi);

end