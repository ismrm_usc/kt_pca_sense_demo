function b_hat = kt_pca_xm(ut)

load('kt_pcadata.mat');
% Basis_tem,nx,ny,nt,idx_re

% INPUT:
% ut: npc*(nx*ny)
% Basis_tem: nt*npc
% OUTPUT:
% b: npc*(nx*ny)
npc=size(Basis_tem,2);

ut=reshape(ut,(nx*ny),npc);% ut: npc*(nx*ny)
ut=ut.';
Array_t= Basis_tem*ut; % nt*(nx*ny)
Array_t=Array_t.';

Im=reshape(Array_t,ny,nx,nt);

K=zeros(ny,nx,nt);
K_un0=zeros(ny,nx,nt);
Im_un0=zeros(ny,nx,nt);

for nnt=1:nt
    
    K(:,:,nnt)=ft(Im(:,:,nnt));
    K_un0(:,:,nnt)=K(:,:,nnt).*idx_re(:,:,nnt);
    
    Im_un0(:,:,nnt)=ift(K_un0(:,:,nnt));
    
end

Im_un=reshape(Im_un0,nx*ny,nt);

Im_un=Im_un.'; %dim: nt*(nx*ny)

b_hat=Basis_tem'*Im_un; % dim: npc*(nx*ny)

b_hat=b_hat(:);

end

